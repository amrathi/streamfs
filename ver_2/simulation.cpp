#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include<unordered_map>
#include<deque>
//#include <cstdio.h>



#define DISK_ACCESS_TIME 50
#define CACHE_ACCESS_TIME 5
#define CACHE_BLOCK_SIZE 10
#define INPUT_SIZE 30
#define PROCESS_COUNT 1

int g_total_time;

using namespace std;
int g_input[PROCESS_COUNT][INPUT_SIZE];
int g_count[PROCESS_COUNT];
int g_current_count[PROCESS_COUNT];

int g_process_serving_disk = -1;

int disk_remaining_time = 0;
int current_process = 0;
int total_trace_count = 0;

void init()
{
	for(int i = 0; i < PROCESS_COUNT; i++)
	{
			g_current_count[i] = 0;
	}
	g_total_time = 0;

}

void scanInput()
{
	for(int i = 0; i < PROCESS_COUNT; i++)
	{
		string line;
		ifstream myfile("input" + std::to_string(i+1));
		if (myfile.is_open())
		{
			int l = 0;
			int temp = 0;
			while ( myfile >> temp )
			{
			  g_input[i][l] = temp;
			  l++;
			  total_trace_count++;
			}
			g_count[i] = l;
			myfile.close();
		}
	}
}

// A FUNCTION TO ADD A PAGE WITH GIVEN 'PAGENUMBER' TO BOTH QUEUE AND HASH
void Enqueue(deque<int>& queue, unordered_map< int,deque<int>::iterator >& hash, int pageNumber)
{
	if( queue.size() == CACHE_BLOCK_SIZE )
	{
		hash.erase(queue.back());
		queue.pop_back();
	}
	
	queue.push_front(pageNumber);
	hash.insert(make_pair(pageNumber,queue.begin()));
}

void run(deque<int>& queue, unordered_map< int,deque<int>::iterator >& hash)
{
	while(total_trace_count > 0)
	{
		if(hash.count(g_input[current_process][g_current_count[current_process]]) == 0)
		{
			if(disk_remaining_time > 0)
			{
				disk_remaining_time--;
				//g_total_time++;
				current_process++;
				if(current_process == PROCESS_COUNT)
					current_process = 0;
				
				if(g_process_serving_disk == current_process)
				{
					current_process++;
					if(current_process == PROCESS_COUNT)
						current_process = 0;
				}
				printf("current process : %d disk not free\n",current_process); 
				
				continue;
			}
			g_process_serving_disk=current_process;
			
			Enqueue( queue, hash, g_input[current_process][g_current_count[current_process]] );
			g_current_count[current_process]++;
			g_total_time += DISK_ACCESS_TIME;
			
			disk_remaining_time = DISK_ACCESS_TIME;

			printf("current process : %d accessing disk\n",current_process); 
				
			current_process++;
			if(current_process == PROCESS_COUNT)
				current_process = 0;
				
			total_trace_count--;
			printf("trace count : %d\n", total_trace_count);
			
		}
		else
		{
			g_total_time += CACHE_ACCESS_TIME;
			//GET THE LOCATION OF PAGE IN CACHE
			deque<int>::iterator it = hash.at(g_input[current_process][g_current_count[current_process]]);
			//IF THE PAGE IS NOT AT FRONT OF CACHE THEN
			if( it != queue.begin() )
			{
				//ERASE THE ENTRY
				queue.erase( it );
				//PUT THE ENTRY AT FRONT OF CACHE
				queue.push_front(g_input[current_process][g_current_count[current_process]]);
				//UPDATE HASH ENTRY FOR THAT PAGE NUMBER
				hash.at(g_input[current_process][g_current_count[current_process]]) = queue.begin();
			}
			
			disk_remaining_time -= CACHE_ACCESS_TIME;
			if(disk_remaining_time < 0)
			{
				disk_remaining_time = 0;
				g_process_serving_disk = -1;
			}
				
			g_current_count[current_process]++;
			
			g_total_time += CACHE_ACCESS_TIME;
			
			printf("current process : %d accessing cache\n",current_process); 
			
			total_trace_count--;
			printf("trace count : %d\n", total_trace_count);
		}
	
	}
	
}

int main()
{
	init();
	g_total_time = 0;
	deque<int> q;
	unordered_map < int,deque<int>::iterator > hash;
	scanInput();
	
	run(q, hash);
	
	printf("total time : %d\n", g_total_time);


}
